#include "arithmatoy.h"

// Fonctions utilitaires

int get_digit(const char* num, int pos, int base) {
    char c = num[pos];
    if (c >= '0' && c <= '9') {
        return c - '0';
    } else if (c >= 'a' && c <= 'z') {
        return c - 'a' + 10;
    } else {
        // erreur : caractère non valide
        return -1;
    }
}

char get_char(int digit, int base) {
    if (digit < 10) {
        return '0' + digit;
    } else {
        return 'a' + digit - 10;
    }
}

// Fonctions arithmétiques

char* arithmatoy_add(const char* num1, const char* num2, int base) {
    int len1 = strlen(num1);
    int len2 = strlen(num2);
    int max_len = (len1 > len2) ? len1 : len2;
    char* result = malloc((max_len + 1) * sizeof(char));
    int carry = 0;

    for (int i = 0; i < max_len; i++) {
        int digit1 = (i < len1) ? get_digit(num1, len1 - 1 - i, base) : 0;
        int digit2 = (i < len2) ? get_digit(num2, len2 - 1 - i, base) : 0;
        int sum = digit1 + digit2 + carry;
        carry = sum / base;
        result[max_len - 1 - i] = get_char(sum % base, base);

        if (VERBOSE) {
            fprintf(stderr, "Adding %c and %c, result is %c, carry is %d\n", 
                    get_char(digit1, base), get_char(digit2, base), 
                    get_char(sum % base, base), carry);
        }
    }

    if (carry > 0) {
        result = realloc(result, (max_len + 2) * sizeof(char));
        result[0] = get_char(carry, base);
        max_len++;
    }

    result[max_len] = '\0';
    return result;
}

char* arithmatoy_sub(const char* num1, const char* num2, int base) {
    int len1 = strlen(num1);
    int len2 = strlen(num2);
    int max_len = (len1 > len2) ? len1 : len2;
    char* result = malloc((max_len + 1) * sizeof(char));
    int borrow = 0;

    for (int i = 0; i < max_len; i++) {
        int digit1 = (i < len1) ? get_digit(num1, len1 - 1 - i, base) : 0;
        int digit2 = (i < len2) ? get_digit(num2, len2 - 1 - i, base) : 0;
        int diff = digit1 - digit2 - borrow;
        borrow = (diff < 0) ? 1 : 0;
        result[max_len - 1 - i] = get_char((diff + base) % base, base);

        if (VERBOSE) {
            fprintf(stderr, "Subtracting %c from %c, result is %c, borrow is %d\n", 
                    get_char(digit2, base), get_char(digit1, base), 
                    get_char((diff + base) % base, base), borrow);
        }
    }

    if (borrow > 0) {
        // erreur : num1 est inférieur à num2
        free(result);
        return NULL;
    }

    result[max_len] = '\0';
    return result;
}

char* arithmatoy_mul(const char* num1, const char* num2, int base) {
    int len1 = strlen(num1);
    int len2 = strlen(num2);
    int max_len = len1 + len2;
    char* result = malloc((max_len + 1) * sizeof(char));
    memset(result, 0, (max_len + 1) * sizeof(char));

    for (int i = 0; i < len1; i++) {
        int digit1 = get_digit(num1, len1 - 1 - i, base);
        for (int j = 0; j < len2; j++) {
            int digit2 = get_digit

char* arithmatoy_mul(const char* num1, const char* num2, int base) {
    int len1 = strlen(num1);
    int len2 = strlen(num2);
    int max_len = len1 + len2;
    char* result = malloc((max_len + 1) * sizeof(char));
    memset(result, 0, (max_len + 1) * sizeof(char));

    for (int i = 0; i < len1; i++) {
        int digit1 = get_digit(num1, len1 - 1 - i, base);
        for (int j = 0; j < len2; j++) {
            int digit2 = get_digit(num2, len2 - 1 - j, base);
            int product = digit1 * digit2;
            int carry = product / base;
            int sum = product % base;
            result[max_len - 1 - i - j] += sum;
            if (result[max_len - 1 - i - j] >= base) {
                result[max_len - 1 - i - j] -= base;
                result[max_len - 1 - i - j - 1] += 1;
            }
            if (carry > 0) {
                result[max_len - 1 - i - j - 1] += carry;
            }

            if (VERBOSE) {
                fprintf(stderr, "Multiplying %c and %c, result is %c, carry is %d\n", 
                        get_char(digit1, base), get_char(digit2, base), 
                        get_char(sum, base), carry);
            }
        }
    }

    // Conversion des chiffres du produit en caractères et ajout à la chaîne de résultat
    for (int i = 0; i < max_len; i++) {
        result[i] = get_char(result[i], base);
    }

    // Suppression des zéros non significatifs
    while (max_len > 1 && result[0] == '0') {
        memmove(result, result + 1, (max_len - 1) * sizeof(char));
        max_len--;
    }

    result[max_len] = '\0';
    return result;
}